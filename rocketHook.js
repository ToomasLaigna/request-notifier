const instanceParams = new Map([
    ['JP-TEST', { color: '#F9B10D', title_link: 'https://some_test_central_server' }],
    ['JP-DEV', { color: '#F9B10D', title_link: 'https://some_dev_central_server' }],
    ['JP', { color: '#F9B10D', title_link: 'https://some_prod_central_server' }]
]);

class Script {

    process_incoming_request({ request }) {
        if (request.content.authRequests) {
            return this.processAuthRequests(request.content.authRequests)
        } else if (request.content.signRequests) {
            return this.processSignRequests(request.content.signRequests)
        } else {
            return {
                error: {
                    success: false,
                    message: "Expected authRequests or signRequests to be the json root."
                }
            };
        }
    }

    processAuthRequests(authRequests) {
        return {
            content: {
                text: ":lock: **New authentication certificate request has arrived:** \n" + this.printCertRequest(authRequests[0], '\n'),
                "attachments": [{
                    "color": this.getExtraParamsForRequest(authRequests[0]).color,
                    "title": "Currently pending authentication certificate requests:",
                    "title_link": this.getExtraParamsForRequest(authRequests[0]).title_link,
                    "text": authRequests.map(this.printCertRequest).join('\n'),
                    "fields": [{
                        "title": "Instance",
                        "value": authRequests[0].instance,
                        "short": false
                    }]
                }]
            }
        };
    }

    processSignRequests(signRequests) {
        return {
            content: {
                text: ":lock: **New sign certificate request has arrived:** \n" + this.printCertRequest(signRequests[0], '\n'),
                "attachments": [{
                    "color": this.getExtraParamsForRequest(signRequests[0]).color,
                    "title": "Currently pending sign certificate requests:",
                    "title_link": this.getExtraParamsForRequest(signRequests[0]).title_link,
                    "text": signRequests.map(this.printCertRequest).join('\n'),
                    "fields": [{
                        "title": "Instance",
                        "value": signRequests[0].instance,
                        "short": false
                    }]
                }]
            }
        };
    }

    getExtraParamsForRequest(request) {
        return instanceParams.get(request.instance);
    }

    printCertRequest(req, delim = ' ') {
        if ("AuthCertRegRequest" === req.requestType) {
            return ["**Request ID:** " + req.id,
            "**Date of Request:** " + req.createdAt,
            "**Security Server Owner Name:** " + req.securityServerOwnerName,
            "**Security Server FQDN:** " + req.securityServerFQDN,
            "**Security Server Address:** " + req.securityServerAddress,
            "**Instance:** " + req.instance]
                .join(delim);
        } else if ("ClientRegRequest" === req.requestType) {
            return ["**Request ID:** " + req.id,
            "**Date of Request:** " + req.createdAt,
            "**Subsystem Owner Name:** " + req.subsystemOwnerName,
            "**Subsystem FQDN:** " + req.subsystemFQDN,
            "**Security Server Owner Name:** " + req.securityServerOwnerName,
            "**Security Server FQDN:** " + req.securityServerFQDN,
            "**Instance:** " + req.instance]
                .join(delim);
        } else {
            throw new Error(`requestType ${certRequest.requestType} should be AuthCertRegRequest or ClientRegRequest`);
        }
    }
}

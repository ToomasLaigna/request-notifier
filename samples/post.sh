#!/bin/bash

TITLE="MyBot"
TITLE_LINK="https://xrd-cs-01.test.avenuecross.com:4000/requests"
IMAGE_URL="https://rocket.chat/images/mockup.png"
COLOR="#764FA5"
TEXT=$(./auth.sh)

#PAYLOAD="{\"username\":\"JP-TEST\",\"text\":\"Request ID\tSecurity Server Owner Name\tSecurity Server FQDN\tSecurity Server Address\tCreated at\n378\tChivit test\tJP-TEST-COM-23259982640509040147\t192.168.122.1\t2018-09-27 03:42:31.884432 (1 row)\",\"attachments\":[{\"title\":\"$TITLE\",\"title_link\":\"$TITLE_LINK\",\"text\":\"TEXT\",\"image_url\":\"$IMAGE_URL\",\"color\":\"$COLOR\"}]}"
#PAYLOAD="{\"username\":\"JP-TEST\",\"text\":\"$TEXT\",\"attachments\":[{\"title\":\"$TITLE\",\"title_link\":\"$TITLE_LINK\",\"text\":\"TEXT\",\"image_url\":\"$IMAGE_URL\",\"color\":\"$COLOR\"}]}"
HOOK="https://chat.planetway.com:9443/hooks/pBhEor7PJZLaWN8r9/3Bf3zXrWBggqbbZgTqp33WMdhhN5pTELFKyyqGeguETpQZsh"
echo $TEXT
curl -X POST -H 'Content-Type: application/json; charset=UTF-8' --data "$TEXT" "$HOOK"
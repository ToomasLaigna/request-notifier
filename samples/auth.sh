#!/bin/bash
su -c "psql -t -P format=unaligned -d centerui_production -c 
\"with t as (
    select 
        r.id as Request_ID, 
        r.server_owner_name as Security_Server_Owner_Name, 
        concat(i.xroad_instance, ':', r.server_owner_class, ':', r.server_owner_code, ':', r.server_code) as Security_Server_FQDN, 
        r.address as Security_Server_Address, 
        to_char(r.created_at, 'DD.MM.YYYY HH:MI:SS') as Created_at, 
        i.xroad_instance as instance, 
        r.type as type_request 
    from requests r 
    join identifiers i on i.id=r.security_server_id 
    where 
        r.processing_status='WAITING' 
        and r.type='AuthCertRegRequest' 
        and r.origin='SECURITY_SERVER' 
    order by r.id DESC)
select json_agg(t) from t;\"" postgres
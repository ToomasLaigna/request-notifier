package com.planetway.data;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class ChatRequestBuilderTests {

    private SignRequestData signRequestData() {
        SignRequestData signRequestData = new SignRequestData();
        signRequestData.id = 1;
        signRequestData.createdAt = "27.06.2019 02:50:56";
        signRequestData.subsystemOwnerName = "Acme Corp.";
        signRequestData.subsystemFQDN = "JP-TEST:COM:12314521:SUBSYS";
        signRequestData.securityServerOwnerName = "Acme Corp.";
        signRequestData.securityServerFQDN = "JP-TEST:COM:2141421:SS";
        signRequestData.instance = "JP-TEST";
        return signRequestData;
    }

    private AuthRequestData authRequestData() {
        AuthRequestData authRequestData = new AuthRequestData();
        authRequestData.id = 1;
        authRequestData.createdAt = "27.06.2019 02:50:56";
        authRequestData.securityServerOwnerName = "Acme Corp.";
        authRequestData.securityServerAddress = "127.0.0.1";
        authRequestData.securityServerFQDN = "JP-TEST:COM:2141421:SS";
        authRequestData.instance = "JP-TEST";
        return authRequestData;
    }

    @Test
    public void testSignRequests() throws Exception {
        List<SignRequestData> signRequestDataList = Arrays.asList(
                signRequestData(),
                signRequestData(),
                signRequestData()
        );

        String s = ChatRequestBuilder.buildSign(signRequestDataList, "https://api.slack.com/");
        Assert.assertEquals("{\"text\":\":satellite_antenna: *New client registration request has arrived in _JP-TEST_:*\\n*Request ID:* 1\\n*Date of Request:* 27.06.2019 02:50:56\\n*Subsystem Owner Name:* Acme Corp.\\n*Subsystem FQDN:* JP-TEST:COM:12314521:SUBSYS\\n*Security Server Owner Name:* Acme Corp.\\n*Security Server FQDN:* JP-TEST:COM:2141421:SS\",\"attachments\":[{\"mrkdwn_in\":[\"text\"],\"color\":\"#36a64f\",\"title\":\"Currently pending client registration requests:\",\"title_link\":\"https://api.slack.com/\",\"text\":\"\\n*Request ID:* 1 *Date of Request:* 27.06.2019 02:50:56 *Subsystem Owner Name:* Acme Corp. *Subsystem FQDN:* JP-TEST:COM:12314521:SUBSYS *Security Server Owner Name:* Acme Corp. *Security Server FQDN:* JP-TEST:COM:2141421:SS\\n*Request ID:* 1 *Date of Request:* 27.06.2019 02:50:56 *Subsystem Owner Name:* Acme Corp. *Subsystem FQDN:* JP-TEST:COM:12314521:SUBSYS *Security Server Owner Name:* Acme Corp. *Security Server FQDN:* JP-TEST:COM:2141421:SS\",\"footer\":\"Notifications\",\"footer_icon\":\"https://platform.slack-edge.com/img/default_application_icon.png\",\"ts\":123}]}",
                s.replaceAll("\"ts\":(\\d+)", "\"ts\":123"));
    }

    @Test
    public void testAuthRequests() throws Exception {
        List<AuthRequestData> authRequestDataList = Arrays.asList(
                authRequestData(),
                authRequestData(),
                authRequestData()
        );

        String s = ChatRequestBuilder.buildAuth(authRequestDataList, "https://api.slack.com/");
        Assert.assertEquals("{\"text\":\":lock: *New authentication certificate request has arrived in _JP-TEST_:*\\n*Request ID:* 1\\n*Date of Request:* 27.06.2019 02:50:56\\n*Security Server Owner Name:* Acme Corp.\\n*Security Server FQDN:* JP-TEST:COM:2141421:SS\\n*Security Server Address:* 127.0.0.1\",\"attachments\":[{\"mrkdwn_in\":[\"text\"],\"color\":\"#36a64f\",\"title\":\"Currently pending authentication certificate registration requests:\",\"title_link\":\"https://api.slack.com/\",\"text\":\"\\n*Request ID:* 1 *Date of Request:* 27.06.2019 02:50:56 *Security Server Owner Name:* Acme Corp. *Security Server FQDN:* JP-TEST:COM:2141421:SS *Security Server Address:* 127.0.0.1\\n*Request ID:* 1 *Date of Request:* 27.06.2019 02:50:56 *Security Server Owner Name:* Acme Corp. *Security Server FQDN:* JP-TEST:COM:2141421:SS *Security Server Address:* 127.0.0.1\",\"footer\":\"Notifications\",\"footer_icon\":\"https://platform.slack-edge.com/img/default_application_icon.png\",\"ts\":123}]}",
                s.replaceAll("\"ts\":(\\d+)", "\"ts\":123"));
    }
}
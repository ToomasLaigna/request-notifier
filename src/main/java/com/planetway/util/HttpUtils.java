package com.planetway.util;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class HttpUtils {

    public static <T> String postJson(String url, String data)
            throws ClientProtocolException, IOException {
        try (CloseableHttpClient client = HttpClients.createDefault()) {            
            HttpPost post = new HttpPost(url);
            post.setEntity(new StringEntity(data, ContentType.APPLICATION_JSON));
            
            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                try (InputStream is = entity.getContent()) {
                    return IOUtils.toString(is, StandardCharsets.UTF_8);
                }
            }
            return null;
        }
    }
}
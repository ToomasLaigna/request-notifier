package com.planetway.util;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ArrayUtils {

    public static List<Integer> split(String str) {
        return Arrays.stream(str.split(",", -1))
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());
    }

    public static String join(List<Integer> list) {
        return String.join(",", list.stream()
                .map(Object::toString)
                .collect(Collectors.toList()));
    }
}
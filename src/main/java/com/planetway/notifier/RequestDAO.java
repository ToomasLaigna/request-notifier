package com.planetway.notifier;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.planetway.data.AuthRequestData;
import com.planetway.data.SignRequestData;

import org.apache.http.client.ClientProtocolException;

public class RequestDAO {

    private final static String signSql = ""
        + "with t as ("
        + "select "
        + "  r.id as Request_ID, "
        + "  r.server_user_name as Subsystem_Owner_Name, "
        + "  concat(i.xroad_instance, ':', i.member_class, ':', i.member_code, ':', i.subsystem_code) as Subsystem_FQDN, "
        + "  r.server_owner_name as Security_Server_Owner_Name, "
        + "  concat(r.server_owner_class, ':', r.server_owner_code, ':', r.server_code) as Security_Server_FQDN, "
        + "  i.xroad_instance as Instance, "
        + "  to_char(r.created_at, 'DD.MM.YYYY HH:MI:SS') as Created_At, "
        + "  r.type as Request_Type "
        + "from requests r "
        + "join identifiers i on i.id=r.sec_serv_user_id "
        + "where "
        + "  r.processing_status='WAITING' "
        + "  and r.type='ClientRegRequest' "
        + "  and r.origin='SECURITY_SERVER' "
        + "order by r.id DESC) "
        + "select * from t;";

    private final static String authSql = ""
        + "with t as ("
        + "select "
        + "  r.id as Request_ID, "
        + "  r.server_owner_name as Security_Server_Owner_Name, "
        + "  concat(i.xroad_instance, ':', r.server_owner_class, ':', r.server_owner_code, ':', r.server_code) as Security_Server_FQDN, "
        + "  r.address as Security_Server_Address, "
        + "  i.xroad_instance as Instance, "
        + "  to_char(r.created_at, 'DD.MM.YYYY HH:MI:SS') as Created_At, "
        + "  r.type as Request_Type "
        + "from requests r "
        + "join identifiers i on i.id=r.security_server_id "
        + "where "
        + "  r.processing_status='WAITING' "
        + "  and r.type='AuthCertRegRequest' "
        + "  and r.origin='SECURITY_SERVER' "
        + "order by r.id DESC)"
        + "select * from t;";

    public List<AuthRequestData> getAuthRequests(Connection connection) throws SQLException, ClientProtocolException, IOException {
        try (Statement stmt = connection.createStatement()) {
            try (ResultSet resultSet = stmt.executeQuery(authSql)) {
                List<AuthRequestData> requestList = new ArrayList<>();
            
                while (resultSet.next()) {
                    AuthRequestData requestData = new AuthRequestData();
                    requestData.id = resultSet.getInt("Request_ID");
                    requestData.instance = resultSet.getString("Instance");
                    requestData.createdAt = resultSet.getString("Created_At");        
                    requestData.requestType = resultSet.getString("Request_Type");
                    requestData.securityServerOwnerName = resultSet.getString("Security_Server_Owner_Name");
                    requestData.securityServerFQDN = resultSet.getString("Security_Server_FQDN");
                    requestData.securityServerAddress = resultSet.getString("Security_Server_Address");                    
                    requestList.add(requestData);
                }

                return requestList;
            }
        }
    }

    public List<SignRequestData> getSignRequests(Connection connection) throws SQLException, ClientProtocolException, IOException {
        try (Statement stmt = connection.createStatement()) {
            try (ResultSet resultSet = stmt.executeQuery(signSql)) {
                List<SignRequestData> requestList = new ArrayList<>();
            
                while (resultSet.next()) {
                    SignRequestData requestData = new SignRequestData();
                    requestData.id = resultSet.getInt("Request_ID");
                    requestData.instance = resultSet.getString("Instance");
                    requestData.createdAt = resultSet.getString("Created_At");
                    requestData.requestType = resultSet.getString("Request_Type");
                    requestData.subsystemOwnerName = resultSet.getString("Subsystem_Owner_Name");
                    requestData.subsystemFQDN = resultSet.getString("Subsystem_FQDN");
                    requestData.securityServerOwnerName = resultSet.getString("Security_Server_Owner_Name");
                    requestData.securityServerFQDN = resultSet.getString("Security_Server_FQDN");
                    requestList.add(requestData);
                }

                return requestList;
            }
        }
    }
}

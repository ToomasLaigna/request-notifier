package com.planetway.notifier;

import com.planetway.data.AuthRequestData;
import com.planetway.data.ChatRequestBuilder;
import com.planetway.data.SignRequestData;
import com.planetway.util.ArrayUtils;
import com.planetway.util.HttpUtils;
import com.planetway.util.SqlUtils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;

public class Main {

    private Properties properties;
    private String dbUri;
    private String dbUser;
    private String dbPassword;
    private String hookUrl;
    private String authTitleLink;
    private String signTitleLink;
    private int interval;
    private List<Integer> prevAuthRequestIds = new ArrayList<>();
    private List<Integer> prevSignRequestIds = new ArrayList<>();
    private RequestDAO requestDAO;

    public static void main(String[] args) throws Exception {
        Main main = new Main();
        try {
            main.run();
        } catch (Exception ex) {
            main.saveResources();
            throw ex;
        }
    }

    public Main() throws Exception {
        loadResources();
        requestDAO = new RequestDAO();
    }

    public void run() throws Exception {
        while (true) {
            readAndPost();
            Thread.sleep(interval);
        }
    }

    private void readAndPost() throws Exception {
        List<AuthRequestData> authRequests;
        List<SignRequestData> signRequests;
        try (Connection conn = SqlUtils.connect(dbUri, dbUser, dbPassword)) {
            authRequests = requestDAO.getAuthRequests(conn);
            signRequests = requestDAO.getSignRequests(conn);
        }
        
        List<AuthRequestData> newAuthRequests = authRequests.stream()
                .filter(x -> !prevAuthRequestIds.contains(x.id))
                .collect(Collectors.toList());
        List<SignRequestData> newSignRequests = signRequests.stream()
                .filter(x -> !prevSignRequestIds.contains(x.id))
                .collect(Collectors.toList());

        if (!newAuthRequests.isEmpty()) {
            String json = ChatRequestBuilder.buildAuth(authRequests, authTitleLink);
            System.out.println("REQ: " + json);
            String response = HttpUtils.postJson(hookUrl, json);
            if (response != null) {
                System.out.println("RESP: " + response);
            }
        }
        if (!newSignRequests.isEmpty()) {
            String json = ChatRequestBuilder.buildSign(signRequests, signTitleLink);
            System.out.println("REQ: " + json);
            String response = HttpUtils.postJson(hookUrl, json);
            if (response != null) {
                System.out.println("RESP: " + response);
            }
        }

        prevAuthRequestIds = authRequests.stream()
                .map(x -> x.id)
                .collect(Collectors.toList());
        prevSignRequestIds = signRequests.stream()
                .map(x -> x.id)
                .collect(Collectors.toList());
    }

    public void loadResources() throws IOException {
        System.out.println("Initializing...");
        properties = new Properties();

        try (InputStream is = new FileInputStream("notifier.properties")) {
            properties.load(is);
        }

        hookUrl = properties.getProperty("hook.url");
        authTitleLink = properties.getProperty("auth.title.link");
        signTitleLink = properties.getProperty("sign.title.link");
        dbUri = properties.getProperty("db.connectionString");
        dbUser = properties.getProperty("db.username");
        dbPassword = properties.getProperty("db.password");
        interval = Integer.parseInt(properties.getProperty("query.interval.milliseconds"));
        
        Optional.ofNullable(properties.getProperty("previous.auth.request.ids"))
                .filter(x -> !"".equals(x))
                .map(ArrayUtils::split)
                .ifPresent(xlist -> {
                    prevAuthRequestIds = xlist;
                });;
        Optional.ofNullable(properties.getProperty("previous.sign.request.ids"))
                .filter(x -> !"".equals(x))
                .map(ArrayUtils::split)
                .ifPresent(xlist -> {
                    prevSignRequestIds = xlist;
                });;

        System.out.println("Loaded previous.auth.request.ids: [" + ArrayUtils.join(prevAuthRequestIds) + "]");
        System.out.println("Loaded previous.sign.request.ids: [" + ArrayUtils.join(prevSignRequestIds) + "]");
    }

    public void saveResources() {
        try {
            properties.setProperty("previous.auth.request.ids", ArrayUtils.join(prevAuthRequestIds));
            properties.setProperty("previous.sign.request.ids", ArrayUtils.join(prevSignRequestIds));
            try (OutputStream os = new FileOutputStream("notifier.properties")) {
                properties.store(os, "notifier.properties");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.err.println("");
        }
    }
}
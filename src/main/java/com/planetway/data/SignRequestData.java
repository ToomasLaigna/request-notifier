package com.planetway.data;

public class SignRequestData extends RequestData {

    public String subsystemOwnerName;
    public String subsystemFQDN;
    public String securityServerOwnerName;
    public String securityServerFQDN;
}

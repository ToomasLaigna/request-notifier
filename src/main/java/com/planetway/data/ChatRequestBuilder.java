package com.planetway.data;

import com.planetway.util.JsonUtils;

import java.time.Instant;
import java.util.List;

import static java.util.Collections.singletonList;

public class ChatRequestBuilder {

    public static String buildAuth(List<AuthRequestData> authDataList, String titleLink) throws Exception {
        String instance = authDataList.get(0).instance;
        String attachmentContent = authDataList.stream()
                .skip(1)
                .map(x -> formatRequest(x, ' '))
                .reduce("", (x, y) -> x + "\n" + y);

        return JsonUtils.toJson(new RequestStructure(
                ":lock: *New authentication certificate request has arrived in _" + instance + "_:*\n"
                        + formatRequest(authDataList.get(0), '\n'),
                singletonList(new Attachment("Currently pending authentication certificate registration requests:", titleLink, attachmentContent))
        ));
    }

    public static String buildSign(List<SignRequestData> signDataList, String titleLink) throws Exception {
        String instance = signDataList.get(0).instance;
        String attachmentContent = signDataList.stream()
                .skip(1)
                .map(x -> formatRequest(x, ' '))
                .reduce("", (x, y) -> x + "\n" + y);

        return JsonUtils.toJson(new RequestStructure(
                ":satellite_antenna: *New client registration request has arrived in _" + instance + "_:*\n"
                        + formatRequest(signDataList.get(0), '\n'),
                singletonList(new Attachment("Currently pending client registration requests:", titleLink, attachmentContent))
        ));
    }

    private static String formatRequest(RequestData requestData, char delim) {
        if (requestData instanceof AuthRequestData) {
            AuthRequestData authRequestData = (AuthRequestData) requestData;
            return "*Request ID:* " + requestData.id + delim +
                    "*Date of Request:* " + requestData.createdAt + delim +
                    "*Security Server Owner Name:* " + authRequestData.securityServerOwnerName + delim +
                    "*Security Server FQDN:* " + authRequestData.securityServerFQDN + delim +
                    "*Security Server Address:* " + authRequestData.securityServerAddress;
        } else if (requestData instanceof SignRequestData) {
            SignRequestData signRequestData = (SignRequestData) requestData;
            return "*Request ID:* " + requestData.id + delim +
                    "*Date of Request:* " + requestData.createdAt + delim +
                    "*Subsystem Owner Name:* " + signRequestData.subsystemOwnerName + delim +
                    "*Subsystem FQDN:* " + signRequestData.subsystemFQDN + delim +
                    "*Security Server Owner Name:* " + signRequestData.securityServerOwnerName + delim +
                    "*Security Server FQDN:* " + signRequestData.securityServerFQDN;
        } else {
            throw new RuntimeException("Unknown request type " + requestData.getClass().getName());
        }
    }

    private static class RequestStructure {

        public String text;
        public List<Attachment> attachments;

        public RequestStructure(String text, List<Attachment> attachments) {
            this.text = text;
            this.attachments = attachments;
        }
    }

    private static class Attachment {

        public List<String> mrkdwn_in = singletonList("text");
        public String color = "#36a64f";
        public String title;
        public String title_link;
        public String text;
        public String footer = "Notifications";
        public String footer_icon = "https://platform.slack-edge.com/img/default_application_icon.png";
        public Long ts = Instant.now().toEpochMilli() / 1000;

        public Attachment(String title, String titleLink, String text) {
            this.title = title;
            this.title_link = titleLink;
            this.text = text;
        }
    }
}

package com.planetway.data;

public class AuthRequestData extends RequestData {

    public String securityServerOwnerName;
    public String securityServerFQDN;
    public String securityServerAddress;
}
